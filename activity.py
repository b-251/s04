# [ABSTRACT]
from abc import ABC, abstractclassmethod
class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass


#
	#[CAT SECTION]
#
class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()

		self._name = name
		self._breed = breed
		self._age = 0


#setter methods
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

# getter methods
	def get_name(self):
		print(f"Name of Person: {self._name}")

	def get_breed(self):
		print(f"Breed of: {self._breed}")

	def get_age(self):
		print(f"Age of: {self._age}")
# other methods
	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self._name}, come on!")

#
	#[DOG SECTION]
#
class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()

		self._name = name
		self._breed = breed
		self._age = 0

#setter methods
	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age

# getter methods
	def get_name(self):
		print(f"Name of Person: {self._name}")

	def get_breed(self):
		print(f"Breed of: {self._breed}")

	def get_age(self):
		print(f"Age of: {self._age}")
# other methods
	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print(f"Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self._name}!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss","Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()